<?php
/**
 * @file
 * Contains \Drupal\testexample\Controller\TestexampleController.
 */
namespace Drupal\testexample\Controller;

/**
 * Testexample page controller.
 */
class TestexampleController {
  /**
   * Print an image with an alt tag via attributes.
   */
  public function content() {
    $build = array();
    $build['intro'] = array(
      '#markup' => t('Oy and vey?'),
    );
    $build['image_with_alt_working'] = array(
      '#theme' => 'image',
      '#uri' => '/core/themes/bartik/logo.png',
      '#alt' => 'Test alt',
      '#title' => 'Test title',
      '#width' => '50%',
      '#height' => '50%',
      '#attributes' => array('class' => 'some-img', 'id' => 'my-img'),
    );
    $build['image_with_alt_attribute_not_working'] = array(
      '#theme' => 'image',
      '#uri' => '/core/themes/bartik/logo.png',
      '#width' => '50%',
      '#height' => '50%',
      '#attributes' => array(
        'class' => 'some-img',
        'id' => 'my-img',
        'title' => 'New test title',
        'alt' => 'New test alt',
      ),
    );
    $build['closing'] = array(
      '#markup' => t('this follows the image'),
    );
    return $build;
  }
}
